package pl.szkolenieandroid.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static android.net.ConnectivityManager.TYPE_WIFI;

public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final String CHANGES = "changes";

    static Boolean connected;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isWifiConnected = activeNetwork != null && activeNetwork.getType() == TYPE_WIFI && activeNetwork.isConnected();
        if (connected == null || connected != isWifiConnected) {
            Log.e("NetworkChangeReceiver", "Changing state to: " + isWifiConnected);
            connected = isWifiConnected;

            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            Set<String> changesSet = defaultSharedPreferences.getStringSet(CHANGES, new HashSet<String>());
            changesSet.add(new Date().toString());
            SharedPreferences.Editor edit = defaultSharedPreferences.edit();
            edit.putStringSet(CHANGES, changesSet);
            edit.commit();
        }
    }
}
