package pl.szkolenieandroid.broadcastreceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by akonior on 18.11.2014.
 */
public class PowerConnectionReceiver extends BroadcastReceiver {

    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("PowerConnectionReceiver", "Power connection changed");
        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("Power connection changed");
        builder.setContentText("Power connection change at " + dateFormat.format(new Date()));

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent activityIntent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, 1, activityIntent, 0);
        builder.setContentIntent(activity);

        notificationManager.notify(1, builder.build());

    }
}
