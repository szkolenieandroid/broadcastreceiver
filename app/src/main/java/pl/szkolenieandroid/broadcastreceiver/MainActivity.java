package pl.szkolenieandroid.broadcastreceiver;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;


public class MainActivity extends Activity {

    private ListView listView;
    private ArrayAdapter<String> adapter;
    List<String> strings = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, strings);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> changes = defaultSharedPreferences.getStringSet(NetworkChangeReceiver.CHANGES, Collections.<String>emptySet());
        strings.clear();
        strings.addAll(changes);
        Collections.sort(strings);
        adapter.notifyDataSetChanged();
    }
}
